//
//  ViewController.swift
//  L4_Dz_1
//
//  Created by Hen Joy on 4/15/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        firstBlock()
        secondBlock()
    }
    
    func firstBlock(){
        let name = "Vladymyr"
        let pass = "Password1230$"
        showStrName()
        checkIfNameOfFatherHaveIch()
        divideNameAndSurname()
        let reversedStr = reverseString(str: name)
        print("Reversed string from \(name) is \(reversedStr)")
        let numberWithDots = addDotsToNumber(num: 123456789)
        print("Nuber with dots \(numberWithDots)")
        let difficult = lvlOfDifficultOfPassword(password: pass)
        print("Difficult of password \(pass) = \(difficult)")
    }
    
    func showStrName(){
        let name = "Vladymyr"
        print("Count letters in word Vladymyr is \(name.count)")
    }
    
    func checkIfNameOfFatherHaveIch(){
        let fatherName = "Vladymyrovich"
        let checkSuf = fatherName.hasSuffix("ich")
        if checkSuf {
            print("Father name has suffix ich")
        }else{
            print("Father name doesn't have suffix ich")
        }
    }
    
    func divideNameAndSurname(){
        let nameSurname = "VladymyrBova" as NSString
        let names = nameSurname.substring(to: 8)
        let surname = nameSurname.substring(from: 8)
        let result = names + " " + surname
        print(result)
    }
    
    func reverseString(str: String) -> String{
        var count = str.count - 1
        var reversed = ""
        for i in 0...str.count - 1 {
            reversed.insert(str[String.Index(encodedOffset: count)], at: String.Index(encodedOffset: i))
            count -= 1
        }
        return reversed
    }
    
    func addDotsToNumber(num: Int) -> String{
        var str = String(num)
        var lenth = str.count
        while true{
            lenth -= 3
            if lenth < 1{
                break
            }
            str.insert(",", at: String.Index(encodedOffset: lenth))
        }
        return str
    }
    
    func lvlOfDifficultOfPassword(password: String) -> Int {
        var lvl = 0
        let numbers = "1234567890"
        let lowLetters = "qwertyuiopasdfghjklzxcvbnm"
        let upLetters = "QWERTYUIOPASDFGHJKLZXCVBNM"
        let specifyCharacters = "!@#$%^&*()_+=-[]{};:|,\\<.>/?~`"
        for i in numbers{
            if password.contains(i){
                lvl += 1
                break
            }
        }
        for i in lowLetters{
            if password.contains(i){
                lvl += 1
                break
            }
        }
        for i in upLetters{
            if password.contains(i){
                lvl += 1
                break
            }
        }
        for i in specifyCharacters{
            if password.contains(i){
                lvl += 1
                break
            }
        }
        if lvl == 4 {
            return 5
        }else{
            return lvl
        }
    }
    
    func secondBlock(){
        let array = [11, 17, 28, 49, 17, 10, 28, 17, 11]
        let arrStr = ["lada", "sedan", "baklazhan"]
        let sortedArray = sortArrayInt(array: array)
        print("Sorted array \(sortedArray)")
        let translitedWord = translitWord(word: "Гусь без жопы")
        print("Word in translite = \(translitedWord)")
        let foundStrings = searchStringsInArrayOfString(array: arrStr, searchingWord: "da")
        print("Found string in array \(arrStr) = \(foundStrings)")
        let withSensorString = antiMat(str: "hello my fak", badWords: ["fuck", "fak"])
        print("String with sensore = \(withSensorString)")
    }
    
    func sortArrayInt(array: [Int]) -> [Int]{
        var sortedArray = [Int]()
        var temp = 0
        for i in array{
            if !sortedArray.contains(i){
                sortedArray.append(i)
            }
        }
        for i in 0..<sortedArray.count {
            for j in (i + 1)..<sortedArray.count {
                if (sortedArray[i] > sortedArray[j])
                {
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[j];
                    sortedArray[j] = temp;
                }
            }
        }
        return sortedArray
    }
    
    func translitWord(word: String) -> String{
        let dictionLow = [
            "а":"a",
            "б":"b",
            "д":"d",
            "е":"e",
            "г":"g",
            "х":"h",
            "и":"i",
            "ж":"zh",
            "к":"k",
            "л":"l",
            "м":"m",
            "н":"n",
            "о":"o",
            "п":"p",
            "ю":"YU",
            "р":"r",
            "с":"s",
            "т":"t",
            "у":"u",
            "в":"v",
            "й":"y",
            "з":"z",
            "ь":"",
            "Ф":"ph",
            "Ц":"z",
            "Ч":"ch",
            "Ш":"sh",
            "Щ":"shcha",
            "Э":"e",
            "Я":"ya",
            "ы":"i",
            " ":" "
        ]
        let dictionUpper = [
            "А":"A",
            "Б":"B",
            "Д":"D",
            "Е":"E",
            "Г":"G",
            "Х":"H",
            "И":"I",
            "Ж":"ZH",
            "К":"K",
            "Л":"L",
            "М":"M",
            "Н":"N",
            "О":"O",
            "П":"P",
            "Ю":"YU",
            "Р":"R",
            "С":"S",
            "Т":"T",
            "У":"U",
            "В":"V",
            "Й":"Y",
            "З":"Z",
            "Ь":"",
            "Ф":"PH",
            "Ц":"Z",
            "Ч":"CH",
            "Ш":"SH",
            "Щ":"SHCHA",
            "Э":"E",
            "Ы":"I",
            "Я":"YA"
        ]
        var result = ""
        var found = 0
        for i in word {
            for j in dictionLow{
                if i == Character(j.key) {
                    result += j.value
                    found += 1
                    break
                }
            }
            if found == 1{
                found = 0
                break
            }
            for j in dictionUpper{
                if i == Character(j.key) {
                    result += j.value
                    found += 1
                    break
                }
            }
        }
        return result
    }
    
    func searchStringsInArrayOfString(array: [String], searchingWord: String) -> [String]{
        var result = [String]()
        for i in array{
            if i.contains(searchingWord){
                result.append(i)
            }
        }
        return result
    }
    
    func antiMat(str: String, badWords: Set<String>) -> String{
        let firstStr = str as NSString
        var result = ""
        for i in badWords{
            if str.contains(i){
                var stars = ""
                for _ in 0..<i.count{
                    stars += "*"
                }
                result = firstStr.replacingOccurrences(of: i, with: stars)
            }
        }
        return result
    }
}

